package BrickBreaker;

import javafx.application.Platform;
import javafx.scene.canvas.GraphicsContext;

/*
* LevelOperator is the controller of levels. It initialize levels and start particular level
* basing on {@link BrickBreaker.Level} menu member.
 */
class LevelOperator {
    // Represents currently selected level or option like exit.
    private int levelNumber = -1;
    // Number of levels available in the game.
    private int availableLevels;

    // This member holds instance of {@link BrickBreaker.LevelMenu} to draw menu and get user's input there.
    private LevelMenu menu;
    // Array of levels available in the game. It's size depends on {@link BrickBreaker.LevelOperator.availableLevels}.
    private Level[] levels;

    LevelOperator(int availableLevels) {
        this.availableLevels = availableLevels;
        menu = new LevelMenu(this.availableLevels);

        initLevels();
    }

    private void initLevels() {
        levels = new Level[availableLevels];
        for(int lvlNumber = 0; lvlNumber < availableLevels; ++lvlNumber) {
            String lvlDefinitionPath = "\\src\\main\\resources\\levelsDefinitions\\level" + (lvlNumber + 1) + ".json";
            levels[lvlNumber] = new Level(lvlDefinitionPath);
        }
    }

    // Determines which level will render
    void levelRender(GraphicsContext graphicsContext, double time) {

        // Menu handler
        if(levelNumber == -1) {
            menu.render(graphicsContext, time);
            return;
        }

        // Exit handler
        if(levelNumber == 0) {
            return;
        }

        levels[levelNumber - 1].render(graphicsContext);
    }

    // Tells the game to perform a logic tick and asks if the level should be exited
    void levelTick(Input input, int tickCount) {

        // Menu handler
        if(levelNumber == -1) {
            menu.activate();
            menu.tick(input);

            if (menu.exit()) {
                menu.deactivate();
                levelNumber = menu.destination();
            }
            return;
        }

        // Exit handler
        if(levelNumber == 0) {
            Platform.exit();
            return;
        }


        // Perform logic for currently selected level
        int currentLvl = levelNumber - 1;

        if (!levels[currentLvl].isActive()) {
            levels[currentLvl].activate();
        }
        if (levels[currentLvl].shouldExit()) {
            levels[currentLvl].exit();
        }
        levels[currentLvl].tick(input, tickCount);

        if (levels[currentLvl].shouldExit()) {
            levels[currentLvl].deactivate();
            levelNumber = levels[currentLvl].destination();
        }
    }
}