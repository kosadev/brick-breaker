package BrickBreaker;

import javafx.scene.image.Image;

public abstract class Entity {
    protected Point position;
    protected Size size;
    protected Image image;

    public int x() {
        return position.getX();
    }

    public int y() {
        return position.getY();
    }

    public int height() {
        return size.getHeight();
    }

    public int width() {
        return size.getWidth();
    }

    public Point middle() {
        return new Point(x() + width() / 2, y() + height() / 2);
    }

    public void setPosition(Point position) {
        this.position = position;
    }

    public void setSize(Size size) {
        this.size = size;
    }

    public int bottomBound() {
        return y() + height();
    }

    public int rightBound() {
        return x() + width();
    }

    public Image getImage() {
        return image;
    }
}
