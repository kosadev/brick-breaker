package BrickBreaker;

import javafx.scene.canvas.Canvas;

public class GameCanvas extends Canvas {

    GameCanvas(double width, double height) {
        super(width, height);
    }

    public void showCanvasDimensions() {
        System.out.println(" Height : " + this.getHeight() + " Width : " + this.getWidth());
    }

    public double GetLayoutXDifference(double layoutX)
    {
        return this.getLayoutX() - layoutX;
    }

    public double GetLayoutYDifference(double layoutY)
    {
        return this.getLayoutY() - layoutY;
    }

    public void setCanvasAsReady() {
        this.autosize();
        this.applyCss();
        this.requestFocus();
    }
}
