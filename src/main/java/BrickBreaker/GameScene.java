package BrickBreaker;

import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.canvas.GraphicsContext;

public class GameScene extends Scene {

    GameCanvas gameCanvas;

    public GameScene(double width, double height, Group root) {
        super(root);

        gameCanvas = new GameCanvas(width, height);
        root.getChildren().add(gameCanvas);
        gameCanvas.setCanvasAsReady();
    }

    GameCanvas getGameCanvas() {
        return gameCanvas;
    }

    GraphicsContext getContext() {
        return  gameCanvas.getGraphicsContext2D();
    }
}
