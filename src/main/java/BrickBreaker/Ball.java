package BrickBreaker;
import javafx.scene.image.Image;

public class Ball extends Entity {
    private int xVec = -optSpeed;
    private int yVec = optSpeed;

    private static int optSpeed = 6;

    private int normalYVec = optSpeed;
    private int slowYVec = 4;
    private int fastYVec = 8;

    private boolean hitFlagX = false;
    private boolean hitFlagY = false;

    private int flagXFrameTimer = 0;
    private int flagYFrameTimer = 0;

    Ball() {
        position = new Point(500, Game.getHeight() - 45 - 20);
        size = new Size(20, 20);
        image = new Image(getClass().getResourceAsStream("/58-Breakout-Tiles.png"));
    }

    private Ball(int xModify, int pointX, int pointY, int xVec, int yVec) {
        position = new Point(pointX, pointY);
        size = new Size(20, 20);
        image = new Image(getClass().getResourceAsStream("/58-Breakout-Tiles.png"));

        this.xVec = xVec + xModify;
        this.yVec = yVec;
        this.normalYVec = 4;
    }

    // Changes position along the y axis
    private boolean yDelta(Paddle player) {

        position.moveX(xVec);

        // Left map border hit
        if (x() < width() / 2) {
            position.setX(width() / 2);
            reflectX();

            // Right map border hit
        } else if (x() > Game.getWidth() - width() / 2) {
            position.setX(Game.getWidth() - (width() / 2));
            reflectX();
        }

        position.moveY(yVec);

        // Upper part of the map hit
        if (y() < height() / 2) {
            position.setY(size.getHeight() / 2);
            reflectY();
            return false;

            // Lower part of the map hit = Death
        } else if (y() > Game.getHeight() - height()) {
            return true;
        }

        // Box Collider (Paddle) and Circle Collider (Ball) collision check
        float dx = Math.max(player.x(), Math.min(middle().getX(), player.rightBound()));
        float dy = Math.max(player.y(), Math.min(middle().getY(), player.bottomBound()));

        dx = middle().getX() - dx;
        dy = middle().getY() - dy;

        if (Math.sqrt(dx * dx + dy * dy) < width() / 2) {

            if (x() + (width() - width() / 10) < player.x() || x() + (width() / 10) > player.rightBound()) {
                if (!hitFlagX) reflectX();
            }

            if (y() + (height() - height() / 10) > player.y() || y() + (height() / 10) < player.bottomBound()) {
                if (!hitFlagY) reflectY();
            }
        }

        // If you hit something you can't invoke hit event for next X frames
        if (hitFlagX) {
            ++flagXFrameTimer;
            if (flagXFrameTimer >= 3) {
                hitFlagX = false;
                flagXFrameTimer = 0;
            }
        }

        if (hitFlagY) {
            ++flagYFrameTimer;
            if (flagYFrameTimer >= 3) {
                hitFlagY = false;
                flagYFrameTimer = 0;
            }
        }

        return false;
    }


    // Position Check
    boolean delta(Paddle player, Boolean onPaddle) {

        if (onPaddle) {
            position.setX(player.middle().getX() - (width() / 2));
            position.setY(player.y() - 20);
            return false;

        } else {
            return yDelta(player);
        }
    }

    public void reflectX() {
        hitFlagX = true;
        xVec = xVec * -1;

        // Randomize Velocity to simulate physics
        if (xVec < 0) xVec = -1 * ((int) (5 * Math.random()) + 2);
        else xVec = ((int) (3 * Math.random()) + 1);

        flagXFrameTimer = 0;
    }

    public void reflectY() {
        hitFlagY = true;
        yVec = yVec * -1;
        flagYFrameTimer = 0;
    }

    void changeSpeed(Booster.BoosterType type) {
        if (type == Booster.BoosterType.SLOW_BALL) {
            if (yVec == normalYVec) {
                yVec = slowYVec;
            } else if (yVec == fastYVec) {
                yVec = normalYVec;
            }
        } else if (type == Booster.BoosterType.FAST_BALL) {
            if (yVec == slowYVec) {
                yVec = normalYVec;

            } else if (width() == normalYVec) {
                yVec = fastYVec;
            }
        }
    }

    void tripleReproduction() {
        int counter = 0;
        for (int i = 0; i < Level.balls.length; i++) {
            if (Level.balls[i] == null) {
                // Randomize other balls movement
                Level.balls[i] = new Ball(counter == 0 ? 1 : -1, this.position.getX(), this.position.getY(), this.xVec, this.yVec);

                // There is a bug when xVec is 0
                if (Level.balls[i].xVec == 0) Level.balls[i].xVec += 2;

                counter++;
            }
        }
    }

    public boolean isHitFlagX() {
        return hitFlagX;
    }

    public boolean isHitFlagY() {
        return hitFlagY;
    }

    public Point crashPoint() {
        return new Point(position.getX() + (width() - width() / 8), position.getY() + (size.getHeight() - size.getHeight() / 8));
    }
}
