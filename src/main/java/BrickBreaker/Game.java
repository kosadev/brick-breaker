/*
 * Purpose: To hold the primary elements of the game application this includes:
 *
                render()
                        > Tells the screen what to draw and where

                tick()
                        > Tells the game to update certain numbers and logic within the game
                        > This includes whether inputs are activated and if physics is
                       applied

    The animation handle
                        > It keeps track of frame rate by watching the computer's internal time

    A level and menu manager
                        > This object tells the game which level to display.
                      It does this by passing the tick() and render() methods
                      through a switch statement which then passes the inputs
                      canvas objects along to an object to implement the level
                      interface (think of it like a train junction telling the
                      game which level to look at)

    The class files of all entities
                        > There will be several objects which can be used to
                       represent data from things like hit boxes and vectors
                        > The delta box is one such object. Its purpose is to
                       describe motion, speed, and position of a box in 2D space
 */

package BrickBreaker;

import javafx.animation.*;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.stage.Stage;

import java.awt.*;

public class Game {
    private int tickCount = 0; // Number of logic updates since start
    private static int width;
    private static int height;

    private Input input;
    private LevelOperator levelOperator;

    Game() {
        width = 1560;
        height = 975;

        levelOperator = new LevelOperator(5);
    }

    // Private Methods
    void run(Stage primaryStage) {
        // Staging block and initialization
        GameStageManager manager = new GameStageManager(width, height, primaryStage);
        input = new Input(manager.getGameScene());

        manager.prepare();
        manager.show();


        // Rendering and logic block
        final long startNanoTime = System.nanoTime();

        new AnimationTimer() {

            int frames = 0;
            int ticks = 0;
            long prevNanos = 0;
            long lastTimer = System.currentTimeMillis();

            //TEMP
            long frameTestTimer = System.currentTimeMillis();

            // End of frame rate block
            @Override
            public void handle(long currentNanoTime) {

                if (currentNanoTime < prevNanos) {
                    return;
                }
                else {
                    prevNanos = currentNanoTime + 10000000;
                }

                double t = (currentNanoTime - startNanoTime) / 1000000000.0;

                tick(input);

                ticks++;

                render(manager.getGameScene().getContext(), t);

                frames++;

                frameTestTimer = System.currentTimeMillis();
                // This resets the performance timer
                if (System.currentTimeMillis() - lastTimer >= 1000) {
                    lastTimer += 1000;
                    System.out.println(frames);
                    frames = 0;
                    ticks = 0;
                }
            }
        }.start();
    }

    private void tick(Input input) {
        tickCount++;
        levelOperator.levelTick(input, tickCount);
    }

    private void render(GraphicsContext graphicContext, double t) {
        levelOperator.levelRender(graphicContext, t);
    }

    static int getHeight() {
        return height;
    }

    public static int getWidth() {
        return width;
    }
}
