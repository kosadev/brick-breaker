package BrickBreaker;
import javafx.scene.image.Image;

class Paddle extends Entity {

    // Booster Sizes
    private static int USUAL_WIDTH = 200;
    private static int BIG_WIDTH = 286;
    private static int SMALL_WIDTH = 95;

    // Vector variables
    private int xVec = 0;
    private static int MAX_SPEAD = 12;
    private static int MIN_SPEED = -12;

    Paddle() {
        setPosition(new Point(0, Game.getHeight() - 45));
        setSize(new Size(45, USUAL_WIDTH));

        image = new Image(getClass().getResourceAsStream("/50-Breakout-Tiles.png"));
    }

    // Moves player along the x axis
    void xDelta() {
        position.moveX(xVec);

        // Paddle can not move more to the left
        if (x() < 0) {
            position.setX(0);
        }

        // Paddle can not move more to the right
        if ((x() + width() > Game.getWidth())) {
            position.setX(Game.getWidth() - width());
        }
    }

    // Block Acceleration

    // Increases movement if the movement is below max speed
    void increaseXVec() {
        if (xVec < MAX_SPEAD) {
            ++xVec;
        }
    }

    // Decreases the x velocity
    void decreaseXVec() {
        if (xVec > MIN_SPEED) {
            --xVec;
        }
    }

    // This applies friction to the paddle if called, slowing it's movement to
    void normalizeX() {

        // Zero
        if (xVec < 0) {
            xVec++;

        } else if (xVec > 0) {
            xVec--;
        }
    }

    void changeSize(Booster.BoosterType type) {
        if (type == Booster.BoosterType.SMALL_PADDLE) {
            if (width() == USUAL_WIDTH) {
                size.setWidth(SMALL_WIDTH);
                image = new Image(getClass().getResourceAsStream("/57-Breakout-Tiles.png"));
            } else if (width() == BIG_WIDTH) {
                size.setWidth(USUAL_WIDTH);
                image = new Image(getClass().getResourceAsStream("/50-Breakout-Tiles.png"));
            }
        } else if (type == Booster.BoosterType.BIG_PADDLE) {
            if (width() == SMALL_WIDTH) {
                size.setWidth(USUAL_WIDTH);
                image = new Image(getClass().getResourceAsStream("/50-Breakout-Tiles.png"));

            } else if (width() == USUAL_WIDTH) {
                size.setWidth(BIG_WIDTH);
                image = new Image(getClass().getResourceAsStream("/56-Breakout-Tiles.png"));
            }
        }
    }
}

