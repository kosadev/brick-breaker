package BrickBreaker;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;

class LevelMenu {
    private boolean isActive;

    private boolean shouldExit = false;
    private boolean toggle = true;

    private int chosenOption = 1;
    private int availableLevels;

    private Image BG_IMAGE = new Image(getClass().getResourceAsStream("/Menu_Background.jpg"));

    LevelMenu(int availableLevels) {
        this.availableLevels = availableLevels;
    }

    // Renders the scene
    void render(GraphicsContext graphicsContext, double time) {

        if (isActive) {
            // Filling background with image
            graphicsContext.drawImage(BG_IMAGE, 0, 0, Game.getWidth(), Game.getHeight());

            int horizontalCenter = Game.getWidth() / 2;

            // Render Title
            graphicsContext.setFill(Color.WHITE);
            graphicsContext.setFont(new Font(80));
            graphicsContext.setTextAlign(TextAlignment.CENTER);

            graphicsContext.fillText("BRICK BREAKER", horizontalCenter, 220);
            graphicsContext.setFont(new Font(55));
            // Renders play and exit signs
            graphicsContext.setFont(new Font(50));

            // Colors the active choice yellow

            for(int i = 1; i<= availableLevels; ++i) {
                graphicsContext.setFill((i == chosenOption) ? Color.YELLOWGREEN : Color.WHITE);
                graphicsContext.fillText("Level " + i, horizontalCenter, 90 * i + 220);
            }

            // if level is selected then draw Exit label in red color. Otherwise in yellow.
            graphicsContext.setFill((chosenOption <= availableLevels) ? Color.WHITE : Color.YELLOWGREEN);
            graphicsContext.fillText("Exit", horizontalCenter, 90 * (availableLevels + 1) + 220);
        }
    }

    // This is for controlling the cursor that selects which level to display
    void tick(Input input) {
        if (this.isActive) {

            // Select should play
            if (input.up && toggle) {
                --chosenOption;
                toggle = false;

            } else if (input.down && toggle) {
                ++chosenOption;
                toggle = false;

            } else if (input.space) {
                shouldExit = true;
            } else if (!input.up && !input.down) {
                toggle = true;
            }

            if (chosenOption == 7) {
                chosenOption = 1;
            } else if (chosenOption == 0) {
                chosenOption = 6;
            }
        }
    }

    // Determines if the level should exit or not to ask the
    // Level operator which level to load next, if destination is zero
    // Then it should close the application
    // As the value of level zero represent an end to the game
    boolean exit() {
        return shouldExit;
    }

    int destination() {
        if(chosenOption <= availableLevels) {
            return chosenOption;
        }

        return 0;
    }

    void activate() {
        this.isActive = true;

        if (shouldExit) {
            this.shouldExit = false;
        }
    }

    void deactivate() {
        this.isActive = false;
    }
}
