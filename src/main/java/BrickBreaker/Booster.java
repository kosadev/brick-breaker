package BrickBreaker;

import javafx.scene.image.Image;

public class Booster extends Entity {

    private int yVec;

    private int optSpeed = 1;

    private BoosterType type;

    public Booster(Point position, BoosterType type) {
        setSize(new Size(20, 76));
        setPosition(position);

        this.yVec = optSpeed;
        this.type = type;

        switch (type) {
            case BIG_PADDLE:
                image = new Image(getClass().getResourceAsStream("/47-Breakout-Tiles.png"));
                break;
            case FAST_BALL:
                image = new Image(getClass().getResourceAsStream("/42-Breakout-Tiles.png"));
                break;
            case SLOW_BALL:
                image = new Image(getClass().getResourceAsStream("/41-Breakout-Tiles.png"));
                break;
            case SMALL_PADDLE:
                image = new Image(getClass().getResourceAsStream("/46-Breakout-Tiles.png"));
                break;
            case TRIPLE_BALL:
                image = new Image(getClass().getResourceAsStream("/43-Breakout-Tiles.png"));
                break;
        }

    }

    // Changes position along the y axis
    boolean yDelta(Paddle player, Ball ball, Boolean movementFlag) {

        if (movementFlag) {
            position.moveY(yVec);
        }

        if (y() > Game.getHeight() - height()) {
            return true;
        }

        // Box Collider & Box Collider collision check
        float ax = (x() + (width() / 2));
        float ay = (y() + (height() / 2));
        float bx = player.rightBound();
        float by = player.bottomBound();

        if (Math.abs(ax - bx) < (width() / 2) + (player.width() / 2)) {
            if (Math.abs(ay - by) < (height() / 2) + (player.height() / 2)) {

                switch (type) {
                    case BIG_PADDLE:
                        player.changeSize(type);
                        break;
                    case FAST_BALL:
                        ball.changeSpeed(type);
                        break;
                    case SLOW_BALL:
                        ball.changeSpeed(type);
                        break;
                    case SMALL_PADDLE:
                        player.changeSize(type);
                        break;
                    case TRIPLE_BALL:
                        for (int i = 0; i < Level.balls.length; i++) {
                            if (Level.balls[i] != null) {
                                Level.balls[i].tripleReproduction();
                                break;
                            }
                        }
                        break;
                }

                return true;
            }
        }

        return false;
    }

    public enum BoosterType {
        BIG_PADDLE,
        FAST_BALL,
        SLOW_BALL,
        SMALL_PADDLE,
        TRIPLE_BALL
    }
}
