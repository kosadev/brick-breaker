/*
 * Purpose:To handle the inputs from the user and bind them to events
 */

package BrickBreaker;

import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.input.KeyEvent;

public class Input {
    // Fields - Determines what values event exist
    public boolean up = false;
    public boolean down = false;
    public boolean left = false;
    public boolean right = false;
    public boolean space = false;
    public boolean m = false;

    Scene scene;

    // Constructor
    public Input(Scene scene) {

        this.scene = scene;
        init();
    }

    // Private Methods
    public void init() {
        // Determines when keys are depressed
        scene.setOnKeyPressed(new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent event) {

                switch (event.getCode()) {

                    case UP:
                        up = true;
                        break;

                    case DOWN:
                        down = true;
                        break;

                    case LEFT:
                        left = true;
                        break;

                    case RIGHT:
                        right = true;
                        break;

                    case SPACE:
                        space = true;
                        break;

                    case M:
                        m = true;
                        break;
                }
            }
        });

        // Determines when keys are not depressed
        scene.setOnKeyReleased(new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent event) {

                switch (event.getCode()) {
                    case UP:
                        up = false;
                        break;

                    case DOWN:
                        down = false;
                        break;

                    case LEFT:
                        left = false;
                        break;

                    case RIGHT:
                        right = false;
                        break;

                    case SPACE:
                        space = false;
                        break;

                    case M:
                        m = false;
                        break;
                }
            }
        });
    }
}
