package BrickBreaker;

import javafx.scene.Group;
import javafx.stage.Stage;

public class GameStageManager extends Group {

    private Stage stage;
    private GameScene scene;
    private static String GAME_NAME = "Brick Breaker";

    GameStageManager(double width, double height, Stage stage) {
        super();
        this.stage = stage;
        stage.setTitle("Brick Breaker");

        scene = new GameScene(width, height, this);
        stage.setScene(scene);
    }

    GameScene getGameScene() {
        return scene;
    }

    void prepare() {
        stage.setFullScreen(true);
        stage.setResizable(false);
    }

    void show() {
        stage.show();
    }
}
