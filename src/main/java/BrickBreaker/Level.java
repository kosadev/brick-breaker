package BrickBreaker;
import BrickBreaker.levels.Brick;
import BrickBreaker.levels.BrickArea;
import BrickBreaker.levels.LevelParser;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/*
 * LevelOperator is the controller of levels. It initialize levels and start particular level
 * basing on {@link BrickBreaker.Level} menu member.
 */
public class Level {
    public static ArrayList<Booster> boosters = new ArrayList<>();

    // Variables
    private boolean ballOnPaddle = true;
    private boolean shouldExit = false;
    private boolean isActive;
    private int livesLeft = 3;
    private int killed = 0;
    private int score = 0;


    private Paddle paddle = new Paddle();
    static Ball[] balls = new Ball[3];

    private static Font FONT = new Font(60);
    private Image BG_IMAGE = new Image(getClass().getResourceAsStream("/BACKGROUND.jpg"));
    private Image HEART_IMAGE = new Image(getClass().getResourceAsStream("/60-Breakout-Tiles.png"));

    private Brick[][] bricks;
    private String levelDefinitionPath;

    // Constructor
    Level(String levelDefinitionPath) {
        this.levelDefinitionPath = levelDefinitionPath;
        activate();
    }

    void render(GraphicsContext graphicsContext) {
        if (isActive) {
            // Filling background with image
            graphicsContext.drawImage(BG_IMAGE, 0, 0, Game.getWidth(), Game.getHeight());

            // Render the position of the player's paddle
            graphicsContext.setFill(Color.ROYALBLUE);
            graphicsContext.drawImage(paddle.getImage(), paddle.x(), paddle.y(), paddle.width(), paddle.height());

            // we render all balls
            for (Ball ball : balls) {
                if (ball != null) {
                    graphicsContext.drawImage(ball.getImage(), ball.x(), ball.y(), ball.width(), ball.height());
                }
            }

            Level.boosters.forEach((booster)->{
                graphicsContext.drawImage(booster.getImage(), booster.x(), booster.y(), booster.width(), booster.height());
            });

            // we render here all bricks from .json definition which are not null and are visible (have color).

            for (Brick[] row : bricks) {
                for (Brick brick : row) {
                    if(brick != null) {
                        if (brick.isVisible() && !brick.isBroken()) {
                            graphicsContext.drawImage(brick.getImage(), brick.x(), brick.y(), brick.width(), brick.height());
                        }
                    }
                }
            }

            // Hearts and score label rendering
            graphicsContext.setFill(Color.LIMEGREEN);
            graphicsContext.drawImage(HEART_IMAGE, 0, 0, 50, 50);
            graphicsContext.setFont(FONT);
            graphicsContext.fillText(Integer.toString(livesLeft - 1), 80, 45);
    }
    }

    void tick(Input input, int tickCount) {
        if (isActive) {

            // When game is in session, Update the paddle object
            if (input.left) // The player wishes to move the paddle left
            {
                paddle.decreaseXVec();

            } else if (input.right) // The player wishes to move to the right
            {
                paddle.increaseXVec();
            }

            // Check if the ball is on the paddle and if the player presses up the ball is released
            if (input.up) // Load ball and fire it
            {
                ballOnPaddle = false;
            }

            // Apply friction to paddle
            if (tickCount % 2 == 0) {
                if (!input.left && !input.right) {
                    paddle.normalizeX();
                }
            }

            // Update all objects:

            // Update the paddle's position:
            paddle.xDelta();

            // Update ball

            boolean movementFlag = true;

            for (int j = 0; j < balls.length; j++) {
                if (balls[j] == null) continue;

                // Decide if the ball should be on the paddle
                if (balls[j].delta(paddle, ballOnPaddle)) {
                    mistake(j);
                }

                killed += checkAll(balls[j]);

                for (int i = 0; i < boosters.size(); i++) {
                    if (boosters.get(i).yDelta(paddle, balls[j], movementFlag)) boosters.remove(i);
                }

                movementFlag = false;
            }
        }
    }

    private int checkAll(Ball ball) {

        int numberKilled = 0;

        for (int i = 0; i < bricks.length; i++) {
            for (int j = 0; j < bricks[i].length; j++) {
                if(bricks[i][j] != null && bricks[i][j].isVisible()) {
                    if(bricks[i][j].detect(ball)) {
                        numberKilled++;
                    }
                }
            }
        }
        return numberKilled;
    }

    private void mistake(int j) {

        int counter = 0;
        for (Ball ball : balls) {
            if (ball == null) {
                counter++;
            }
        }

        if (counter < 2) {
            balls[j] = null;
            return;
        }

        this.livesLeft--;

        if (this.livesLeft == 0) {
            boosters.clear();
            shouldExit = true;
        }

        this.ballOnPaddle = true;
    }

    void activate() {
        boosters.clear();
        this.livesLeft = 3;
        this.isActive = true;
        this.score = 0;

        BrickArea brickArea = LevelParser.parseFile(levelDefinitionPath);
        bricks = brickArea.bricks();

        // removing unnecessary balls and leaving only one
        Arrays.fill(balls, null);
        balls[0] = new Ball();
    }

    void exit() {
        Arrays.fill(balls, null);
        balls[0] = new Ball();

        boosters.clear();
        this.livesLeft = 3;
        shouldExit = false;
        ballOnPaddle = true;
        this.score = 0;
    }

    int destination() {
        return -1;
    }

    void deactivate() {
        this.isActive = false;
    }

    boolean isActive() {
        return isActive;
    }

    boolean shouldExit() {
        return shouldExit;
    }
}
