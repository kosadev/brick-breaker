package BrickBreaker.levels;

import BrickBreaker.Point;
import BrickBreaker.Size;
import BrickBreaker.Game;

public class BrickArea {
    private int spotsInRow = 0;
    private Row[] rows = {};

    public Brick[][] bricks() {
        Brick[][] bricks = new Brick[rows.length][spotsInRow];

        int brickWidth = 120;
        int brickHeight = 40;
        for(int i = 0; i < rows.length; ++i) {
            for(int j = 0; j < rows[i].bricks.length && j < spotsInRow; ++j){
                Brick brick = rows[i].bricks[j];
                if(brick.isVisible()) {
                    brick.init(new Point((j * brickWidth) , (i * brickHeight)), new Size(brickHeight, brickWidth));
                }
                bricks[i][j] = brick;
            }
        }

        return bricks;
    }

    static class Row {
        private int[] corruptedSpots = {};
        private Brick[] bricks = {};

        int[] getCorruptedSpots() {
            return corruptedSpots;
        }

        Brick[] getBricks() {
            return bricks;
        }
    }
}
