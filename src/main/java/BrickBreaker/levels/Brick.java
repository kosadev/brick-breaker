package BrickBreaker.levels;
import BrickBreaker.*;
import javafx.scene.image.Image;

public class Brick extends Entity {
    private boolean isBroken = false;

    private int healthPoints = 1;
    private int colorNumber = 0;

    private boolean loot = false;
    private boolean isSquare = false;
    private Booster.BoosterType lootType;

    public Brick(Point position, Size size, int healthPoints, boolean isSquare, int colorNum, boolean loot, Booster.BoosterType lootType) {

        if (isSquare) {
            size.setWidth(size.getHeight());
            position.moveX(size.getWidth());
        }

        setSize(size);
        setPosition(position);

        this.healthPoints = healthPoints;
        this.colorNumber = colorNum;

        image = new Image(getClass().getResourceAsStream("/bricks/" + (colorNumber < 10 ? "0" + colorNumber : colorNumber) + "-Breakout-Tiles.png"));

        this.loot = loot;
        this.lootType = lootType;
    }

    void init(Point position, Size size) {

        if(isSquare) {
            size.setWidth(size.getHeight());
            position.moveX(size.getWidth());
        }

        setPosition(position);
        setSize(size);

        image = new Image(getClass().getResourceAsStream("/bricks/" + (colorNumber < 10 ? "0" + colorNumber : colorNumber) + "-Breakout-Tiles.png"));
    }

    private boolean hit() {
        healthPoints--;
        colorNumber++;
        image = new Image(getClass().getResourceAsStream("/bricks/" + (colorNumber < 10 ? "0" + colorNumber : colorNumber) + "-Breakout-Tiles.png"));

        if (healthPoints <= 0) {
            healthPoints = 0;
            isBroken = true;

            if (loot) Level.boosters.add(new Booster(new Point(x(), y()), lootType));

            return true;
        }

        return false;
    }

    public boolean detect(Ball ball) {

        if (ball == null) return false;

        if (!isBroken) {

            Point ballMiddle = ball.middle();

            float dx = Math.max(x(), Math.min(ballMiddle.getX(), rightBound()));
            float dy = Math.max(y(), Math.min(ballMiddle.getY(), bottomBound()));

            dx = ballMiddle.getX() - dx;
            dy = ballMiddle.getY() - dy;

            boolean hitCheck = false;

            if (Math.sqrt(dx * dx + dy * dy) < ball.width() / 2) {

                Point ballCrashPoint = ball.crashPoint();

                if (ballCrashPoint.getX() < x() || ballCrashPoint.getX() > rightBound()) {
                    if (!ball.isHitFlagX()) {
                        ball.reflectX();
                        hitCheck = true;
                    }
                }

                if (ballCrashPoint.getY() > y() || ballCrashPoint.getY() < bottomBound()) {
                    if (!ball.isHitFlagY()) {
                        ball.reflectY();
                        hitCheck = true;
                    }
                }

                if (hitCheck) hit();
                return true;
            }
        }
        return false;
    }

    public boolean isBroken() {
        return isBroken;
    }
    public boolean isVisible() {
        return colorNumber != 0;
    }
}
