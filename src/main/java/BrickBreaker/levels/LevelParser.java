package BrickBreaker.levels;

import com.google.gson.Gson;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class LevelParser {

    public static BrickArea parseFile(String path) {
        String fileContent = fileContent(path);

        Gson gson = new Gson();
        return gson.fromJson(fileContent, BrickArea.class);
    }

    private static String fileContent(String filePath)
    {
        String absolutePath = new File("").getAbsolutePath() + filePath;

        StringBuilder contentBuilder = new StringBuilder();
        try (Stream<String> stream = Files.lines( Paths.get(absolutePath), StandardCharsets.UTF_8))
        {
            stream.forEach(s -> contentBuilder.append(s).append("\n"));
        }
        catch (IOException e)
        {
            return "";
        }
        return contentBuilder.toString();
    }
}
